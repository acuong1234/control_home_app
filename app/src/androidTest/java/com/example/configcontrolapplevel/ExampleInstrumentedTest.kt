package com.example.configcontrolapplevel

import androidx.test.InstrumentationRegistry
import androidx.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented up_level, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under up_level.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.example.configcontrolapp", appContext.packageName)
    }
}
