package com.example.configcontrolapplevel.View.DangNhapDangKiActivity

import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.configcontrolapplevel.Model.User
import com.example.configcontrolapplevel.R
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_dang_ki_acitivy.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class DangKiAcitivy : AppCompatActivity() {
    var nameHome: String = ""
    var alertDialogWait: AlertDialog? = null
    var alertDialogSuccess: AlertDialog? = null
    var myRef: DatabaseReference? = null
    var isRegistering: Boolean = false
    var stringDate: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dang_ki_acitivy)

        val db : FirebaseDatabase = FirebaseDatabase.getInstance()
        myRef = db.getReference()

        nameHome = getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currenthome","")
        btn_dangki.setOnClickListener {
            dangki()
        }

        myRef!!.child(nameHome).child("RFID").child("state").addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("Not yet implemented")
            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.getValue(Int::class.java) == 1 && isRegistering){
                    alertDialogWait!!.dismiss()
                    val builder = androidx.appcompat.app.AlertDialog.Builder(this@DangKiAcitivy)
                    val email = edtinput_id.text.toString()
                    myRef!!.child(nameHome).child("RFID").child("card").child(email).setValue(stringDate.toLong())
                    myRef!!.child(nameHome).child("RFID").child("curId").child(email).setValue(null)
                    builder.setTitle("Thành công")
                    builder.setMessage("Bạn đã đăng kí thành công")
                    builder.setPositiveButton("Xác nhận", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            isRegistering = false
                            finish()
                        }
                    })
                    alertDialogSuccess = builder.create()
                    alertDialogSuccess!!.show()
                }
            }

        })
    }

    private fun dangki() {
        val email = edtinput_id.text.toString()
        val password = edtinput_pass.text.toString()
        val fullName = edtinput_name.text.toString()
        val address = edtinput_address.text.toString()
        if (email.length != 0 && password.length != 0 && address.length != 0 && fullName.length != 0) {
            val formatter = SimpleDateFormat("ddMMyyyyHHmmssSS")
            val date = Date()
            stringDate  = formatter.format(date)
            var hexfromdate : ArrayList<Int> = ArrayList()
            for(i in 0..15){
                hexfromdate.add(("0"+stringDate.get(i).toString()).toInt())
            }
            val user = User(email, password, fullName,0,address,1,nameHome)
            myRef!!.child("Users").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(@NonNull dataSnapshot: DataSnapshot) {
                    var tmp: Int =  0
                    for (snapshot in dataSnapshot.children) {
                        val user = snapshot.getValue(User::class.java)
                        if (email.equals(user!!.mId)) {
                            tmp++
                        }
                    }
                    if(tmp == 0){
                        myRef!!.child("Users").child(email).child("mHexId").setValue(hexfromdate)
                        myRef!!.child("Users").child(email).child("mAccount").setValue(user)
                        myRef!!.child(nameHome).child("RFID").child("state").setValue(0)//chế độ đọc
                        myRef!!.child(nameHome).child("RFID").child("curId").child(email).setValue(hexfromdate)
                        isRegistering = true
                        val builder = AlertDialog.Builder(this@DangKiAcitivy)
                        val view : View = LayoutInflater.from(this@DangKiAcitivy).inflate(R.layout.custom_dialog_wait_signup,null,false)
                        builder.setView(view)
                        alertDialogWait = builder.create()
                        alertDialogWait!!.show()
                    }else{
                        Toast.makeText(applicationContext, "Tên đăng nhập đã có người dùng", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onCancelled(@NonNull databaseError: DatabaseError) {

                }
            })
        }else{

            Toast.makeText(applicationContext, "Vui lòng điền đầy đủ thông tin", Toast.LENGTH_SHORT).show()
        }

    }
}
