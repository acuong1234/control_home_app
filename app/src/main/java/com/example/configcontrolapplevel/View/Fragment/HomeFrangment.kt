package com.example.configcontrolapplevel.View.Fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.AlertDialog
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.configcontrolapplevel.Adapter.ItemHomeAdapter
import com.example.configcontrolapplevel.Adapter.RecyclerViewClick
import com.example.configcontrolapplevel.Model.Device
import com.example.configcontrolapplevel.Model.HUser
import com.example.configcontrolapplevel.Model.Room
import com.example.configcontrolapplevel.Model.User
import com.example.configcontrolapplevel.R
import com.example.configcontrolapplevel.View.Room.RoomActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.*
import java.text.Normalizer
import java.util.*
import java.util.regex.Pattern
import kotlin.collections.ArrayList

class HomeFrangment : Fragment() {
    var layoutMain : NestedScrollView? = null
    var layoutOption: FrameLayout? = null
    var layoutManager : RecyclerView.LayoutManager? = null
    var adapter : ItemHomeAdapter? = null
    var arrayList = ArrayList<Room?>()
    var recyclerView : RecyclerView? = null
    var database : FirebaseDatabase = FirebaseDatabase.getInstance()
    var myRef : DatabaseReference = database.getReference()
    var btnOption : FloatingActionButton ? = null
    var btnAdd : FloatingActionButton ? = null
    var btnClear : FloatingActionButton ? = null
    var btnVoice : FloatingActionButton ? = null
    var txtTemp : TextView? = null
    var txtHum : TextView? = null
    var txtHello : TextView? = null
    var txtNoRoom : TextView? = null
    var id: String? = ""
    var nameHome: String? =""
    private val REQ_CODE_SPEECH_INPUT = 100
    var result: ArrayList<String> = ArrayList()
    @SuppressLint("RestrictedApi")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.home_fragment,container,false)
        recyclerView = view.findViewById(R.id.list_item_home)
        btnOption = view.findViewById(R.id.btn_option)
        btnAdd = view.findViewById(R.id.btn_add)
        btnClear = view.findViewById(R.id.btn_clear)
        btnVoice = view.findViewById(R.id.btn_voice)
        txtTemp = view.findViewById(R.id.txt_temp)
        txtHum = view.findViewById(R.id.txt_hum)
        txtHello = view.findViewById(R.id.txt_hello)
        txtNoRoom = view.findViewById(R.id.txt_no_room)
        layoutMain = view.findViewById(R.id.layout_main)
        layoutOption = view.findViewById(R.id.layout_option)

        nameHome = activity!!.getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currenthome","")
        id = activity!!.getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currentuser","")

        btnOption!!.setOnClickListener {
            btnOption!!.visibility = View.INVISIBLE
            layoutOption!!.visibility = View.VISIBLE
            layoutMain!!.isClickable = false
            layoutOption!!.isClickable = true
//            layoutOption!!.foreground.alpha = 230
            layoutOption!!.bringToFront()
            btnAdd!!.bringToFront()
            btnClear!!.bringToFront()
            btnVoice!!.bringToFront()
        }

        btnAdd!!.setOnClickListener {
            addRoom()
        }

        btnClear!!.setOnClickListener {
            btnOption!!.visibility = View.VISIBLE
            layoutOption!!.visibility = View.INVISIBLE
            layoutMain!!.isClickable = true
            layoutOption!!.isClickable = false
        }

        btnVoice!!.setOnClickListener {
            promptSpeechInput()
        }

        getTempHum()
        getHello()
        getRecyclerView()
        getHome()

        return view
    }

    private fun promptSpeechInput() {
        val intent: Intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM)
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault())
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, "Nói gì đó ...")
        try {
            startActivityForResult(intent, REQ_CODE_SPEECH_INPUT)
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(context, "Không nhận diện được", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQ_CODE_SPEECH_INPUT && resultCode == Activity.RESULT_OK && null != data) {
            result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            val thread: Thread = Thread(Runnable {
                try {
                    Thread.sleep(1500)
                } catch (e: Exception) {

                } finally {
                    activity!!.runOnUiThread(Runnable {
                    })
                }
            })
            thread.start()

            val string = result.get(0)
            if(string.equals("bật đèn", true)){
                myRef.child(id!!).child("1BedRoom").child("Sleep Lamp").child("mstatus").setValue(1)
            }
            if(string.equals("tắt đèn", true )){
                myRef.child(id!!).child("1BedRoom").child("Sleep Lamp").child("mstatus").setValue(0)
            }
        }
    }

    fun getHello(){
        myRef.child("Users").child(id!!).addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.getValue(HUser::class.java) != null){
                    val name = p0.getValue(HUser::class.java)!!.mAccount.mName
                    txtHello!!.setText("Hi "+name)
                }
            }

        })
    }

    fun getTempHum(){
        myRef.child(nameHome!!).child("tem").addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.getValue(Int::class.java) != null){
                    txtTemp!!.setText(p0.getValue(Int::class.java)!!.toString()+"°C")
                }
            }

        })
        myRef.child(nameHome!!).child("hum").addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.getValue(Int::class.java) != null){
                    txtHum!!.setText(p0.getValue(Int::class.java)!!.toString()+"%")
                }
            }

        })
    }

    fun getHome(){
        myRef.child(nameHome!!).child("Room").addValueEventListener(object :ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {

            }
            override fun onDataChange(p0: DataSnapshot) {
                arrayList.clear()
                for(postSnapshot in p0.children){
                    val room = postSnapshot.getValue(Room::class.java)
                    arrayList.add(room)
                }
                if(arrayList.size == 0){
                    txtNoRoom!!.visibility = View.VISIBLE
                }else{
                    txtNoRoom!!.visibility = View.INVISIBLE
                }
                adapter!!.notifyDataSetChanged()
            }
        })
    }

    fun addRoom(){
        var newRoom = Room()
        var builder = AlertDialog.Builder(context)
        val view : View = LayoutInflater.from(context).inflate(R.layout.custom_dialog_add_room,null,false)
        val spinnerTypeRoom : Spinner = view.findViewById(R.id.spinner_dialog_type)
        val edtNameRoom : EditText = view.findViewById(R.id.edt_dialog_name)
        val btnConfirm : Button = view.findViewById(R.id.btn_dialog_add)

        ArrayAdapter.createFromResource(context,R.array.type_room,android.R.layout.simple_spinner_item).also { arrayAdapter ->
            arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            spinnerTypeRoom.adapter = arrayAdapter
        }

        builder.setView(view)
        val alertDialog = builder.create()
        alertDialog.show()

        spinnerTypeRoom.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position){
                    0 -> {
                        newRoom.mtype = "Living"
                    }
                    1 -> {
                        newRoom.mtype = "Bed"
                    }
                    2 -> {
                        newRoom.mtype = "Kitchen"
                    }
                    3 -> {
                        newRoom.mtype = "Stair"
                    }
                    4 -> {
                        newRoom.mtype = "Gate"
                    }
                }
            }
        }

        btnConfirm.setOnClickListener {
            if(edtNameRoom.text.toString().length!= 0){
                newRoom.mname = edtNameRoom.text.toString()
                newRoom.mid = arrayList.size
                myRef.child("Home1").child("Room").child(newRoom.mid.toString()+newRoom.mname).setValue(newRoom)
                if(newRoom.mtype.equals("Stair")){
                    var newDevice : Device? = null
                    newDevice = Device()
                    newDevice.mType = 0
                    newDevice.mName = "Đèn cầu thang"
                    newRoom.mnumberdevice = 1
                    myRef.child(nameHome!!).child(newRoom.mid.toString()+newRoom.mname).child(newDevice.mName).setValue(newDevice)
                }
                myRef.child(nameHome!!).child("Room").child(newRoom.mid.toString()+newRoom.mname).child("mnumberdevice").setValue(newRoom.mnumberdevice)
                alertDialog.dismiss()
            }else{
                Toast.makeText(context,"Chưa nhập tên phòng",Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun unAccent(s: String): String {
        val temp = Normalizer.normalize(s, Normalizer.Form.NFD)
        val pattern: Pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+")
        return pattern.matcher(temp).replaceAll("")
    }

    @SuppressLint("WrongConstant")
    fun getRecyclerView(){
        layoutManager = GridLayoutManager(
            context,
            2,
            GridLayoutManager.VERTICAL,
            false
        )
        adapter = ItemHomeAdapter(context,arrayList,object :RecyclerViewClick{
            override fun onMyLongPress(position: Int) {
                var alertDialog : androidx.appcompat.app.AlertDialog? = null
                val builderDelete = androidx.appcompat.app.AlertDialog.Builder(context!!)
                builderDelete.setTitle("Xóa phòng")
                builderDelete.setMessage("Bạn muốn xóa "+arrayList.get(position)!!.mname +" ?")
                builderDelete.setPositiveButton("Xác nhận", object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        myRef.child(nameHome!!).child("Room").child(arrayList.get(position)!!.mid.toString()+arrayList.get(position)!!.mname).setValue(null)
                        myRef.child(nameHome!!).child(arrayList.get(position)!!.mid.toString()+arrayList.get(position)!!.mname).setValue(null)
                        alertDialog!!.dismiss()
                    }
                })
                builderDelete.setNegativeButton("Hủy", object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        alertDialog!!.dismiss()
                    }
                })
                alertDialog = builderDelete.create()
                alertDialog.show()
            }

            override fun onMyClick(position: Int) {
                val intent = Intent(context, RoomActivity::class.java)
                intent.putExtra("room",arrayList.get(position))
                startActivity(intent)
            }
        })
        recyclerView!!.adapter = adapter
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.isFocusable = false
        recyclerView!!.isNestedScrollingEnabled = true
    }
}