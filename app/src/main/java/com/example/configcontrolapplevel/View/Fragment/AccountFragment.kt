package com.example.configcontrolapplevel.View.Fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.configcontrolapplevel.Model.HUser
import com.example.configcontrolapplevel.Model.User
import com.example.configcontrolapplevel.R
import com.google.firebase.database.*

class AccountFragment : Fragment() {
    var id: String? = ""
    var txtName : TextView? = null
    var txtSex : TextView? = null
    var txtRole : TextView? = null
    var txtAddress : TextView? = null
    var database : FirebaseDatabase = FirebaseDatabase.getInstance()
    var myRef : DatabaseReference = database.getReference()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.account_fragment,container,false)
        txtRole = view.findViewById(R.id.txt_role)
        txtName = view.findViewById(R.id.txt_account_name)
        txtSex = view.findViewById(R.id.txt_sex)
        txtAddress = view.findViewById(R.id.txt_address)
        id = activity!!.getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currentuser","")
        myRef.child("Users").child(id!!).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.getValue(HUser::class.java) != null){
                    val user = p0.getValue(HUser::class.java)
                    if(user!!.mAccount.mClassify == 0){
                        txtRole!!.setText("Chủ nhà")
                    }else{
                        txtRole!!.setText("Thành viên")
                    }
                    txtName!!.setText(user.mAccount.mName)
                    txtAddress!!.setText(user.mAccount.mLocation)
                    if(user.mAccount.mSex == 0){
                        txtSex!!.setText("Nam")
                    }else{
                        txtSex!!.setText("Nữ")
                    }
                }
            }

        })
        return view
    }
}