package com.example.configcontrolapplevel.View.Fragment

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.configcontrolapplevel.Adapter.RecyclerViewClick
import com.example.configcontrolapplevel.Adapter.itemSettingAdapter
import com.example.configcontrolapplevel.Model.ItemSetting
import com.example.configcontrolapplevel.R
import com.example.configcontrolapplevel.View.ConfigActivity.ConfigActivity
import com.example.configcontrolapplevel.View.DangNhapDangKiActivity.DangKiAcitivy
import com.example.configcontrolapplevel.View.DangNhapDangKiActivity.DangNhapActivity
import com.example.configcontrolapplevel.View.Room.RoomActivity

class SettingFragment : Fragment() {
    var layoutManager : RecyclerView.LayoutManager? = null
    var adapter : itemSettingAdapter? = null
    var arrayList = ArrayList<ItemSetting>()
    var recyclerView : RecyclerView? = null
    var curPermission : Int = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view : View = inflater.inflate(R.layout.setting_fragment,container,false)
        recyclerView = view.findViewById(R.id.list_setting)

        curPermission = activity!!.getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getInt("currentpermission",-1)

        val itemSettingConfig = ItemSetting(R.drawable.ic_wifi_tethering_white_24dp,"ConfigWifi Esp8266")
        val itemSettingAdd = ItemSetting(R.drawable.ic_add_white_24dp,"Thêm thành viên")
        val itemSettingSupport = ItemSetting(R.drawable.ic_error_outline_white_24dp,"Hỗ trợ")
        val itemSettingLogOut = ItemSetting(R.drawable.ic_power_settings_new_black_24dp,"Đăng xuất")
        arrayList.add(itemSettingConfig)
        arrayList.add(itemSettingAdd)
        arrayList.add(itemSettingSupport)
        arrayList.add((itemSettingLogOut))

        getRecyclerView()
        return view
    }

    fun getRecyclerView(){
        layoutManager = LinearLayoutManager(context)
        adapter = itemSettingAdapter(context, arrayList,object : RecyclerViewClick {
            override fun onMyLongPress(position: Int) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onMyClick(position: Int) {
                val item = arrayList.get(position)
                if(item.title.equals("ConfigWifi Esp8266")){
                    val intent = Intent(context,ConfigActivity::class.java)
                    startActivity(intent)
                }
                if(item.title.equals("Thêm thành viên")){
                    if(curPermission == 0){
                        val intent1 = Intent(context, DangKiAcitivy::class.java)
                        startActivity(intent1)
                    }else if (curPermission == -1){
                        Toast.makeText(context,"Chưa có quyền hiện hành",Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(context,"Chỉ có chủ nhà mới sử dụng được tính năng này",Toast.LENGTH_SHORT).show()
                    }
                }
                if(item.title.equals("Đăng xuất")){
                    var alertDialog : androidx.appcompat.app.AlertDialog? = null
                    val builderDelete = androidx.appcompat.app.AlertDialog.Builder(context!!)
                    builderDelete.setTitle("Đăng xuất")
                    builderDelete.setMessage("Bạn muốn đăng xuất khỏi ứng dụng?")
                    builderDelete.setPositiveButton("Xác nhận", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            val sharedPreferences = activity!!.getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE)
                            val edit = sharedPreferences.edit()
                            edit.putString("currentuser", "")
                            edit.commit()

                            val intent = Intent(context,DangNhapActivity::class.java)
                            startActivity(intent)
                        }
                    })
                    builderDelete.setNegativeButton("Hủy", object : DialogInterface.OnClickListener{
                        override fun onClick(dialog: DialogInterface?, which: Int) {
                            alertDialog!!.dismiss()
                        }
                    })
                    alertDialog = builderDelete.create()
                    alertDialog.show()
                }
            }
        })
        recyclerView!!.adapter = adapter
        recyclerView!!.layoutManager = layoutManager
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.isNestedScrollingEnabled = true
        recyclerView!!.isFocusable = false
    }
}