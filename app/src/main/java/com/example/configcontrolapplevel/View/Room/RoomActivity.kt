package com.example.configcontrolapplevel.View.Room

import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.*
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.configcontrolapplevel.Adapter.ItemDeviceRoomAdapter
import com.example.configcontrolapplevel.Adapter.RecyclerViewClick
import com.example.configcontrolapplevel.Model.Device
import com.example.configcontrolapplevel.Model.Room
import com.example.configcontrolapplevel.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_room.*
import org.w3c.dom.Text

@Suppress("DEPRECATION")
class RoomActivity : AppCompatActivity() {

    var selectedRoom : Room? = null
    val db : FirebaseDatabase = FirebaseDatabase.getInstance()
    val myRef : DatabaseReference = db.getReference()
    var adapter : ItemDeviceRoomAdapter? = null
    var linearLayoutManager : LinearLayoutManager? = null
    val listDevice : ArrayList<Device> = ArrayList()
    var newDevice : Device? = null
    var id: String? = ""
    var nameHome: String? = ""
    var timer: CountDownTimer? = null
    var timerList: ArrayList<CountDownTimer> = ArrayList()
    var btnAddDevice: FloatingActionButton? = null
    var btnSetup: FloatingActionButton? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_room)

        id = getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currentuser","")
        nameHome = getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currenthome","")

        val intent = intent
        selectedRoom = intent.getSerializableExtra("room") as Room?
        Log.d("testnum",selectedRoom!!.mnumberdevice.toString())

        getToolbar()
        getRecyclerView()
        getListDevice()
        btnAddDevice = findViewById(R.id.btn_add_device)
        btnSetup = findViewById(R.id.btn_setup)

        if(selectedRoom!!.mtype.equals("Stair")){
            btnAddDevice!!.visibility = View.INVISIBLE
            btnAddDevice!!.isClickable = false
            btnSetup!!.visibility = View.VISIBLE
        }else{
            btnSetup!!.visibility = View.INVISIBLE
            btnSetup!!.isClickable = false
            btnAddDevice!!.visibility = View.VISIBLE
        }

        btn_add_device.setOnClickListener {
            addDeviceRoom()
        }

        btn_setup.setOnClickListener {
            setupLightStair()
        }
    }

    fun getToolbar(){
        if(selectedRoom!!.mtype.equals("Living")){
            tb_room.setLogo(R.drawable.ic_tv_white_24dp)
        }else if(selectedRoom!!.mtype.equals("Bed")){
            tb_room.setLogo(R.drawable.ic_hotel_white_24dp)
        }else{
            tb_room.setLogo(R.drawable.ic_kitchen_white_24dp)
        }
        tb_room.title = selectedRoom!!.mname
        setSupportActionBar(tb_room)
    }

    fun setupLightStair(){
        val builder = AlertDialog.Builder(this)
        val view : View = LayoutInflater.from(this).inflate(R.layout.custom_dialog_setup,null,false)
        val spinnerMode : Spinner = view.findViewById(R.id.spinner_dialog_mode)
        val btnConfirm : Button = view.findViewById(R.id.btn_dialog_setup)
        var curMode : Int = -1
        val hour : EditText = view.findViewById(R.id.edt_hour_setup)
        val min : EditText = view.findViewById(R.id.edt_min_setup)
        val sec : EditText = view.findViewById(R.id.edt_sec_setup)
        var totalTime: Int = 0

        ArrayAdapter.createFromResource(this,R.array.mode,android.R.layout.simple_spinner_item).also {arrayAdapter ->
            arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            spinnerMode.adapter = arrayAdapter
        }

        spinnerMode.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position){
                    0 -> curMode = 0
                    1 -> curMode = 1
                }
            }
        }

        builder.setView(view)
        val alertDialog = builder.create()
        alertDialog.show()

        btnConfirm.setOnClickListener {
            myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child("Đèn cầu thang").child("mmode").setValue(curMode)

            var hourInput: Int? = 0
            var minInput: Int? = 0
            var secInput: Int? = 0
            if (hour.text.length == 0) {
                hourInput = 0
            } else {
                hourInput = Integer.parseInt(hour.text.toString())
            }
            if (min.text.length == 0) {
                minInput = 0
            } else {
                minInput = Integer.parseInt(min.text.toString())
            }
            if (sec.text.length == 0) {
                secInput = 0
            } else {
                secInput = Integer.parseInt(sec.text.toString())
            }

            totalTime = (hourInput * 60 * 60 + minInput * 60 + secInput )
            myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child("Đèn cầu thang").child("mtime").setValue(totalTime)
            alertDialog.dismiss()
        }

    }

    fun addDeviceRoom(){
        newDevice = Device()
        val builder = AlertDialog.Builder(this)
        val view : View = LayoutInflater.from(this).inflate(R.layout.custom_dialog_add_device,null,false)
        val spinnerDevice : Spinner = view.findViewById(R.id.spinner_dialog_type_device)
        val edtNameDevice : EditText = view.findViewById(R.id.edt_dialog_name_device)
        val btnConfirm : Button = view.findViewById(R.id.btn_dialog_add_device)

        ArrayAdapter.createFromResource(this,R.array.type_device,android.R.layout.simple_spinner_item).also {arrayAdapter ->
            arrayAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item)
            spinnerDevice.adapter = arrayAdapter
        }
        spinnerDevice.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when(position){
                    0 -> newDevice!!.mType = 0
                    1 -> newDevice!!.mType = 1
                }
            }

        }

        builder.setView(view)
        val alertDialog = builder.create()
        alertDialog.show()

        btnConfirm.setOnClickListener {
            if(edtNameDevice.text.toString().length!= 0){
                newDevice!!.mName = edtNameDevice.text.toString()
                selectedRoom!!.mnumberdevice += 1
                myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(newDevice!!.mName).setValue(newDevice)
                myRef.child(nameHome!!).child("Room").child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child("mnumberdevice").setValue(selectedRoom!!.mnumberdevice)
                alertDialog.dismiss()
            }else{
                Toast.makeText(this,"Chưa nhập tên thiết bị",Toast.LENGTH_SHORT).show()
            }
        }
    }

    fun getListDevice(){
        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }
            override fun onDataChange(p0: DataSnapshot) {
                listDevice.clear()
                for(postSnapshot in p0.children){
                    val device = postSnapshot.getValue(Device::class.java)
                    listDevice.add(device!!)
                    timerList.add(object : CountDownTimer(0,0){
                        override fun onFinish() {
                            TODO("Not yet implemented")
                        }

                        override fun onTick(millisUntilFinished: Long) {
                            TODO("Not yet implemented")
                        }
                    })
                    adapter!!.notifyDataSetChanged()
                }
                if(listDevice.size == 0){
                    no_device.visibility = View.VISIBLE
                    list_device.visibility = View.INVISIBLE
                }else{
                    no_device.visibility = View.INVISIBLE
                    list_device.visibility = View.VISIBLE
                }
            }
        })
    }

    fun getRecyclerView(){
        adapter = ItemDeviceRoomAdapter(this,listDevice,object: RecyclerViewClick{
            override fun onMyLongPress(position: Int) {
                var alertDialog : AlertDialog? = null
                val builderDelete = AlertDialog.Builder(this@RoomActivity)
                builderDelete.setTitle("Xóa thiết bị")
                builderDelete.setMessage("Bạn muốn xóa "+listDevice.get(position).mName +" ?")
                builderDelete.setPositiveButton("Xác nhận", object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        selectedRoom!!.mnumberdevice -= 1
                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).setValue(null)
                        myRef.child(nameHome!!).child("Room").child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child("mnumberdevice").setValue(selectedRoom!!.mnumberdevice)
                        alertDialog!!.dismiss()
                    }
                })
                builderDelete.setNegativeButton("Hủy", object : DialogInterface.OnClickListener{
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        alertDialog!!.dismiss()
                    }
                })
                alertDialog = builderDelete.create()
                alertDialog.show()
            }

            override fun onMyClick(position: Int) {

                val builder1 = AlertDialog.Builder(this@RoomActivity)
                val view1 = LayoutInflater.from(this@RoomActivity).inflate(R.layout.custom_dialog_device,null,false)
                val btnOn : Button = view1.findViewById(R.id.btn_dialog_on)
                val btnOff : Button = view1.findViewById(R.id.btn_dialog_off)
                val btnTimer : Button = view1.findViewById(R.id.btn_dialog_hengio)
                val btnLevel : Button = view1.findViewById(R.id.btn_dialog_level)
                builder1.setView(view1)
                val alertDialog1 = builder1.create()
                alertDialog1.show()
                btnOn.setOnClickListener {
                    myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mstatus").setValue(1)
                    myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(3)
                    alertDialog1.dismiss()
                }
                btnOff.setOnClickListener {
                    myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mstatus").setValue(0)
                    myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(0)
                    alertDialog1.dismiss()
                }
                btnLevel.setOnClickListener {
                    alertDialog1.dismiss()
                    val builder3 = AlertDialog.Builder(this@RoomActivity)
                    val view3 = LayoutInflater.from(this@RoomActivity).inflate(R.layout.custom_dialog_mucdo,null,false)
                    val btnInc : Button = view3.findViewById(R.id.btn_inc_level)
                    val btnDec : Button = view3.findViewById(R.id.btn_dec_level)
                    val txtLv1: TextView = view3.findViewById(R.id.btn_lv1)
                    val txtLv2: TextView = view3.findViewById(R.id.btn_lv2)
                    val txtLv3: TextView = view3.findViewById(R.id.btn_lv3)
                    val txtLv4: TextView = view3.findViewById(R.id.btn_lv4)
                    var curLevel : Int = 0

                    btnInc.setOnClickListener {
                        if(curLevel<5&&curLevel>=0){
                            if(curLevel!=1){
                                curLevel--
                                myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(curLevel)
                            }
                        }
                        if(curLevel == -1){
                            myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(4)
                        }
                    }

                    btnDec.setOnClickListener {
                        if(curLevel<=5&&curLevel>0){
                            curLevel++
                            myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(curLevel)
                        }
                        if(curLevel == 5){
                            myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(0)
                        }
                    }

                    myRef.child(nameHome!!).
                    child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).
                    child(listDevice.get(position).mName).child("mlevel").addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {

                        }

                        override fun onDataChange(p0: DataSnapshot) {
                            curLevel = p0.getValue(Int::class.java)!!
                            when(curLevel){
                                0 -> {
                                    txtLv1.setBackgroundResource(R.drawable.bottom_level_contaier_unselected);
                                    txtLv2.setBackgroundColor(Color.parseColor("#52000000"));
                                    txtLv3.setBackgroundColor(Color.parseColor("#52000000"));
                                    txtLv4.setBackgroundResource(R.drawable.top_level_container_unselected);
                                }
                                4 -> {
                                    txtLv1.setBackgroundResource(R.drawable.bottom_level_container_selected);
                                    txtLv2.setBackgroundColor(Color.parseColor("#52000000"));
                                    txtLv3.setBackgroundColor(Color.parseColor("#52000000"));
                                    txtLv4.setBackgroundResource(R.drawable.top_level_container_unselected);
                                }
                                3 -> {
                                    txtLv1.setBackgroundResource(R.drawable.bottom_level_container_selected);
                                    txtLv2.setBackgroundColor(Color.parseColor("#CBFFFFFF"));
                                    txtLv3.setBackgroundColor(Color.parseColor("#52000000"));
                                    txtLv4.setBackgroundResource(R.drawable.top_level_container_unselected);
                                }
                                2 -> {
                                    txtLv1.setBackgroundResource(R.drawable.bottom_level_container_selected);
                                    txtLv2.setBackgroundColor(Color.parseColor("#CBFFFFFF"));
                                    txtLv3.setBackgroundColor(Color.parseColor("#CBFFFFFF"));
                                    txtLv4.setBackgroundResource(R.drawable.top_level_container_unselected);
                                }
                                1 -> {
                                    txtLv1.setBackgroundResource(R.drawable.bottom_level_container_selected);
                                    txtLv2.setBackgroundColor(Color.parseColor("#CBFFFFFF"));
                                    txtLv3.setBackgroundColor(Color.parseColor("#CBFFFFFF"));
                                    txtLv4.setBackgroundResource(R.drawable.top_level_container_selected);
                                }
                            }
                        }
                    })

                    builder3.setView(view3)
                    val alertDialog3 = builder3.create()
                    alertDialog3.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                    alertDialog3.show()

                    txtLv1.setOnClickListener {
                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(4)
//                        txtLv1.setBackgroundResource(R.drawable.bottom_level_container_selected);
//                        txtLv2.setBackgroundColor(Color.parseColor("#52000000"));
//                        txtLv3.setBackgroundColor(Color.parseColor("#52000000"));
//                        txtLv4.setBackgroundResource(R.drawable.top_level_container_unselected);
                    }
                    txtLv2.setOnClickListener {
                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(3)
//                        txtLv1.setBackgroundResource(R.drawable.bottom_level_container_selected);
//                        txtLv2.setBackgroundColor(Color.parseColor("#34B3AFAF"));
//                        txtLv3.setBackgroundColor(Color.parseColor("#52000000"));
//                        txtLv4.setBackgroundResource(R.drawable.top_level_container_unselected);
                    }
                    txtLv3.setOnClickListener {
                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(2)
//                        txtLv1.setBackgroundResource(R.drawable.bottom_level_container_selected);
//                        txtLv2.setBackgroundColor(Color.parseColor("#34B3AFAF"));
//                        txtLv3.setBackgroundColor(Color.parseColor("#34B3AFAF"));
//                        txtLv4.setBackgroundResource(R.drawable.top_level_container_unselected);
                    }
                    txtLv4.setOnClickListener {
                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(1)
//                        txtLv1.setBackgroundResource(R.drawable.bottom_level_container_selected);
//                        txtLv2.setBackgroundColor(Color.parseColor("#34B3AFAF"));
//                        txtLv3.setBackgroundColor(Color.parseColor("#34B3AFAF"));
//                        txtLv4.setBackgroundResource(R.drawable.top_level_container_selected);
                    }
                }
                btnTimer.setOnClickListener {
                    alertDialog1.dismiss()
                    val builder2 = AlertDialog.Builder(this@RoomActivity)
                    val view2 = LayoutInflater.from(this@RoomActivity).inflate(R.layout.custom_dialog_hengio,null,false)
                    val btnStart : Button = view2.findViewById(R.id.btn_start)
                    val btnStop : Button = view2.findViewById(R.id.btn_stop)
                    val btnPause : Button = view2.findViewById(R.id.btn_pause)
                    val hour : EditText = view2.findViewById(R.id.edt_hour)
                    val min : EditText = view2.findViewById(R.id.edt_min)
                    val sec : EditText = view2.findViewById(R.id.edt_sec)
                    var leftTime: Long? = 0
                    var hourLeft: Long? = 0
                    var minLeft: Long? = 0
                    var secLeft: Long? = 0

                    btnPause.visibility = View.INVISIBLE
                    btnStop.visibility = View.INVISIBLE

                    builder2.setView(view2)
                    val alertDialog2 = builder2.create()
                    alertDialog2.show()



                    myRef.child(nameHome!!).
                        child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).
                        child(listDevice.get(position).mName).child("mcountDown").addValueEventListener(object : ValueEventListener {
                        override fun onCancelled(p0: DatabaseError) {

                        }
                        override fun onDataChange(p0: DataSnapshot) {
                            leftTime =  p0.getValue(Long::class.java)
                            if(leftTime!! > 0L){
                                hourLeft = leftTime!! / 1000 / 60 / 60
                                minLeft = (leftTime!! / 1000 - hourLeft!! * 60 * 60) / 60
                                secLeft = leftTime!! / 1000 - hourLeft!! * 60 * 60 - minLeft!! * 60
                                hour.setText(hourLeft.toString())
                                min.setText(minLeft.toString())
                                sec.setText(secLeft.toString())
                                btnStart.setText("Resume")
                                btnStart.isClickable = true
                                btnPause.visibility = View.VISIBLE
                                btnStop.visibility = View.VISIBLE
                            }else if(leftTime!! == 0L){
                                hour.setText("")
                                min.setText("")
                                sec.setText("")
                                hour.hint = "0"
                                min.hint = "0"
                                sec.hint = "0"
                                btnStop.visibility = View.INVISIBLE
                                btnPause.visibility = View.INVISIBLE
                                btnStart.isClickable = true
                                btnStart.setText("Start")
                                myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mcountDown").setValue(0)
                            }else {
                                timer!!.cancel()
                                myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mcountDown").setValue(0)
                            }
                        }
                    })

                    btnStart.setOnClickListener {
                        btnStart.setText("Resume")
                        btnStart.isClickable = false
                        btnPause.visibility = View.VISIBLE
                        btnStop.visibility = View.VISIBLE
                        hour.isEnabled = false
                        min.isEnabled = false
                        sec.isEnabled = false
                        var hourInput: Int? = 0
                        var minInput: Int? = 0
                        var secInput: Int? = 0
                        if (hour.text.length == 0) {
                            hourInput = 0
                        } else {
                            hourInput = Integer.parseInt(hour.text.toString())
                        }
                        if (min.text.length == 0) {
                            minInput = 0
                        } else {
                            minInput = Integer.parseInt(min.text.toString())
                        }
                        if (sec.text.length == 0) {
                            secInput = 0
                        } else {
                            secInput = Integer.parseInt(sec.text.toString())
                        }

                        leftTime = ((hourInput * 60 * 60 + minInput * 60 + secInput) * 1000 ).toLong()


                        if(leftTime!! >= 0L){
                            myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mcountDown").setValue(leftTime)
                            timer = object : CountDownTimer(leftTime!!, 1000) {
                                override fun onFinish() {
                                    btnStop.visibility = View.INVISIBLE
                                    btnPause.visibility = View.INVISIBLE
                                    btnStart.isClickable = true
                                    btnStart.setText("Start")
                                    hour.setText("")
                                    min.setText("")
                                    sec.setText("")
                                    hour.isEnabled = true
                                    min.isEnabled = true
                                    sec.isEnabled = true
                                    hour.hint = "0"
                                    min.hint = "0"
                                    sec.hint = "0"

                                    if(listDevice.get(position).mStatus==0){
                                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mstatus").setValue(1)
                                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(3)

                                    }else{
                                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mstatus").setValue(0)
                                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mlevel").setValue(0)

                                    }
                                    myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mcountDown").setValue(0)


                                    val vibrator = this@RoomActivity?.getSystemService(Context.VIBRATOR_SERVICE) as Vibrator
                                    if (Build.VERSION.SDK_INT >= 26) {
                                        vibrator.vibrate(VibrationEffect.createOneShot(1000, VibrationEffect.DEFAULT_AMPLITUDE))
                                    } else {
                                        vibrator.vibrate(1000)
                                    }
                                    alertDialog2.dismiss()
                                }

                                override fun onTick(millisUntilFinished: Long) {
                                    hourLeft = millisUntilFinished / 1000 / 60 / 60
                                    minLeft = (millisUntilFinished / 1000 - hourLeft!! * 60 * 60) / 60
                                    secLeft = millisUntilFinished / 1000 - hourLeft!! * 60 * 60 - minLeft!! * 60
                                    hour.setText(hourLeft.toString())
                                    min.setText(minLeft.toString())
                                    sec.setText(secLeft.toString())
                                    leftTime = leftTime!! - 1000
                                    if(leftTime!! >= 0){
                                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mcountDown").setValue(leftTime)
                                    }else{
                                        timer!!.cancel()
                                    }
//                                myRef.child("countdown"+selectedRoom!!.mid+listDevice.get(position).mName).setValue(millisUntilFinished / 1000)1000
                                }
                            }
//                        timerList.add(position, timer as CountDownTimer)
//                        timerList.get(position).start()

                            timer!!.start()
                        }
                    }

                    btnStop.setOnClickListener {
                        alertDialog2.dismiss()
                        myRef.child(nameHome!!).child(selectedRoom!!.mid.toString()+selectedRoom!!.mname).child(listDevice.get(position).mName).child("mcountDown").setValue(0)
                        timer!!.cancel()
//                        timerList.get(position).cancel()
//                        timerList.removeAt(position)
//                        leftTime = 0
//                        hour.setText("")
//                        min.setText("")
//                        sec.setText("")
//                        hour.hint = "0"
//                        min.hint = "0"
//                        sec.hint = "0"
//                        btnStop.visibility = View.INVISIBLE
//                        btnPause.visibility = View.INVISIBLE
//                        btnStart.isClickable = true
//                        btnStart.setText("Start")
//                        hour.isEnabled = true
//                        min.isEnabled = true
//                        sec.isEnabled = true
                    }

                    btnPause.setOnClickListener {
//                        timerList.get(position).cancel()
                        timer!!.cancel()
                        btnStart.isClickable = true
                    }
                }

            }

        })

        linearLayoutManager = LinearLayoutManager(this)
        list_device.adapter = adapter
        list_device.layoutManager = linearLayoutManager
        list_device.setHasFixedSize(true)
    }

}
