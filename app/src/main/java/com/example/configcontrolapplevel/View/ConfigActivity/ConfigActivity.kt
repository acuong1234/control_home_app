package com.example.configcontrolapplevel.View.ConfigActivity

import android.Manifest
import android.content.*
import android.net.wifi.ScanResult
import android.net.wifi.WifiManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.example.configcontrolapplevel.R
import kotlinx.android.synthetic.main.activity_config.*
import android.content.IntentFilter
import android.util.Log
import androidx.core.app.ActivityCompat
import android.content.pm.PackageManager
import android.os.AsyncTask
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AlertDialog
import android.view.LayoutInflater
import android.widget.*
import com.example.configcontrolapplevel.Model.DownloadJson
import com.example.configcontrolapplevel.View.TrangChuActivity.Main2Activity
import com.google.firebase.database.*

class ConfigActivity : AppCompatActivity() {
    var wifiManager : WifiManager? = null
    var listWifi = ArrayList<String>()
    var adapter : ArrayAdapter<String>? = null
    var id: String? = ""
    var wifiReceiver = object : BroadcastReceiver(){
        override fun onReceive(context: Context?, intent: Intent?) {

            val results:List<ScanResult> = wifiManager!!.scanResults
            Log.d("TestWifi","up_level")
            unregisterReceiver(this)
            Log.d("TestWifi",results.size.toString())
            for (i in 0 until results.size) {
                listWifi.add(results.get(i).SSID)
                Log.d("TestWifi",results.get(i).SSID)
                adapter!!.notifyDataSetChanged()
            }

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_config)

        getPermisson()

        id = getSharedPreferences("id", AppCompatActivity.MODE_PRIVATE).getString("currentuser","")

        wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager?

        adapter = ArrayAdapter(this,android.R.layout.simple_list_item_1,listWifi)
        list_wifi.adapter = adapter

        checkConnect()

        btn_connect.setOnClickListener {
            wifiManager!!.setWifiEnabled(true)
            view_unableWifi.visibility = View.INVISIBLE
            list_wifi.visibility = View.VISIBLE
//            val wifiInfo: WifiInfo = wifiManager!!.connectionInfo
//            if(wifiInfo!=null){
//                txt_wifi_connected.setText(wifiInfo.ssid)
//            }
            scanWifi()
        }

        list_wifi.setOnItemClickListener { parent, view, position, id ->
            val builder = AlertDialog.Builder(this)
            val view = LayoutInflater.from(this).inflate(R.layout.custom_dialog_configwifi,null,false)
            val ssid : TextView = view.findViewById(R.id.dialog_txt_ssid)
            val pass : EditText = view.findViewById(R.id.dialog_edt_pass)
            val config : Button = view.findViewById(R.id.dialog_btn_config)
            ssid.append(listWifi.get(position))
            config.setOnClickListener {
                configWifi(ssid.text.toString(),pass.text.toString())
            }
            builder.setView(view)
            val alertDialog = builder.create()
            alertDialog.show()
        }
    }
    class config(
        var context: Context,
        var progressDialog: AlertDialog
    ) : AsyncTask<String, Void, Boolean>() {

        override fun onPreExecute() {
            super.onPreExecute()
            progressDialog.show()
        }

        override fun doInBackground(vararg params: String?): Boolean {
            val ssid = params[0]
            val pass = params[1]
            val url = "http://192.168.4.1/wifisave?s=" + ssid + "&p=" + pass
            Log.d("TestESP", url)
            val downloadJson = DownloadJson(url, null)
            downloadJson.execute()
            return true
        }

    }

    fun getPermisson(){
        if (ContextCompat.checkSelfPermission(
                this@ConfigActivity,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(
                this@ConfigActivity,
                Manifest.permission.CHANGE_WIFI_STATE
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            //Request permission
            ActivityCompat.requestPermissions(
                this@ConfigActivity,
                arrayOf(
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_WIFI_STATE,
                    Manifest.permission.CHANGE_WIFI_STATE,
                    Manifest.permission.ACCESS_NETWORK_STATE
                ),
                123
            )
        }
    }

    fun checkConnect(){
        if(!wifiManager!!.isWifiEnabled){
            view_unableWifi.visibility = View.VISIBLE
            list_wifi.visibility = View.INVISIBLE
//            ln_connected.visibility = View.INVISIBLE
        }else{
            view_unableWifi.visibility = View.INVISIBLE
            list_wifi.visibility = View.VISIBLE
//            ln_connected.visibility = View.VISIBLE
//            val wifiInfo: WifiInfo = wifiManager!!.connectionInfo
//            if(wifiInfo!=null){
//                txt_wifi_connected.setText(wifiInfo.ssid)
//            }
            scanWifi()
        }
    }

    fun scanWifi() {
        listWifi.clear()
        registerReceiver(wifiReceiver, IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION))
        @Suppress("DEPRECATION")
        wifiManager!!.startScan()
        Toast.makeText(this, "Scanning WiFi ...", Toast.LENGTH_SHORT).show()
    }

    fun configWifi(ssid: String, pass: String) {
        val database: FirebaseDatabase = FirebaseDatabase.getInstance()
        val myRef = database.getReference()

        val progressDialogBuider = AlertDialog.Builder(this)
        val view = LayoutInflater.from(this).inflate(R.layout.custom_dialog, null)
        progressDialogBuider.setView(view)
        progressDialogBuider.setCancelable(false)
        val progressDialog = progressDialogBuider.create()

        val c: config = config(applicationContext,progressDialog!!)
        c.execute(ssid, pass)

        myRef.child(id!!).child("connected").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {

            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.getValue(Int::class.java) == 1) {
                    progressDialog.dismiss()
                    Toast.makeText(applicationContext, "Esp kết nối thành công đến Wifi", Toast.LENGTH_SHORT).show()
                    val intent: Intent = Intent(applicationContext, Main2Activity::class.java)
                    startActivity(intent)
                }
                Log.d("TestFB", p0.getValue(Int::class.java).toString())
            }
        })
    }
}
