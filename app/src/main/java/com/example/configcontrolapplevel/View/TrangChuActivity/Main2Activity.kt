@file:Suppress("DEPRECATION")

package com.example.configcontrolapplevel.View.TrangChuActivity

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.configcontrolapplevel.R
import com.example.configcontrolapplevel.View.Fragment.AccountFragment
import com.example.configcontrolapplevel.View.Fragment.HomeFrangment
import com.example.configcontrolapplevel.View.Fragment.SettingFragment
import com.google.firebase.database.*

class Main2Activity : AppCompatActivity() {
    var id : String = ""
    var nameHome: String = ""

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        val fragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        var fragment: Fragment? = null
        when (item.itemId) {
            R.id.navigation_home -> {
                fragment = HomeFrangment()
                fragmentTransaction.replace(R.id.layout_fragment, fragment)
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_account -> {
                fragment = AccountFragment()
                fragmentTransaction.replace(R.id.layout_fragment, fragment)
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_setting -> {
                fragment = SettingFragment()
                fragmentTransaction.replace(R.id.layout_fragment, fragment)
                fragmentTransaction.commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val db: FirebaseDatabase = FirebaseDatabase.getInstance()
        val myRef: DatabaseReference = db.getReference()

        nameHome = getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currenthome","")
        id = getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currentuser","")

        myRef.child(nameHome).child("tem").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if(p0.getValue(Int::class.java) != null){
                    val temp: Int = p0.getValue(Int::class.java)!!
                    if (temp > 30) {
                        createNoti(temp)
                    }
                }
            }

        })

        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

//        val mBottomNavigationMenuView : BottomNavigationMenuView = navView.getChildAt(0) as BottomNavigationMenuView
//        val view : BottomNavigationItemView = mBottomNavigationMenuView.getChildAt(1) as BottomNavigationItemView
//        val viewHome = LayoutInflater.from(applicationContext).inflate(R.layout.custom_home_bottom_item,null,false)
//        view.addView(viewHome)

        navView.selectedItemId = R.id.navigation_home
    }


    fun createNotiChannel(){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            val name : String = "NotiChannel"
            val desc : String = "MyChannel"
            val important = NotificationManager.IMPORTANCE_DEFAULT
            val channel : NotificationChannel = NotificationChannel("MYAPP_01",name,important)
            channel.description = desc
            val notificationManager : NotificationManager = getSystemService(NotificationManager::class.java)
            notificationManager.createNotificationChannel(channel)
        }
    }

    @SuppressLint("WrongConstant")
    fun createNoti(nhietdo: Int) {
        createNotiChannel()

        val intent1 = Intent(this, Main2Activity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }

        val penddingintent: PendingIntent = PendingIntent.getActivity(this, 0, intent1, 0)

        val builder =
            NotificationCompat.Builder(this, "MYAPP_01")
                .setSmallIcon(R.drawable.ic_error_white_24dp)
                .setContentTitle("Nhiệt độ trong nhà khá cao. Hãy kiểm tra")
                .setContentText("Nhiệt độ :" + nhietdo.toString() +" °C")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(penddingintent).setAutoCancel(true)
        with(NotificationManagerCompat.from(this)) {
            notify(12, builder.build())
        }
    }

    override fun onBackPressed() {
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }
}
