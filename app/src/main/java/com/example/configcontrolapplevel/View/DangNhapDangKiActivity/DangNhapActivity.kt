package com.example.configcontrolapplevel.View.DangNhapDangKiActivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.annotation.NonNull
import com.example.configcontrolapplevel.Model.HUser
import com.example.configcontrolapplevel.Model.User
import com.example.configcontrolapplevel.R
import com.example.configcontrolapplevel.View.TrangChuActivity.Main2Activity
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_dang_nhap.*



class DangNhapActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dang_nhap)

        val id = getSharedPreferences("home", AppCompatActivity.MODE_PRIVATE).getString("currentuser","")
        if(id.length!=0){
            val intent = Intent(applicationContext, Main2Activity::class.java)
            startActivity(intent)
        }

        btn_dangnhap.setOnClickListener {
            dangnhap()
        }

//        btn_noacc.setOnClickListener {
//            val intent1 = Intent(this, DangKiAcitivy::class.java)
//            startActivityForResult(intent1, 99)
//        }
    }

    private fun dangnhap() {
        val db : FirebaseDatabase = FirebaseDatabase.getInstance()
        val myRef : DatabaseReference = db.getReference()
        val email = edt_email.getText().toString()
        val password = edt_pass.getText().toString()
        if(email.length!=0 && password.length!=0){
            myRef.child("Users").addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(@NonNull dataSnapshot: DataSnapshot) {
                    var tmp: Int = 0
                    for (snapshot in dataSnapshot.children) {
                        val user = snapshot.getValue(HUser::class.java)
                        if (email == user!!.mAccount.mId && password == user.mAccount.mPass) {
                            tmp++

                            val sharedPreferences = getSharedPreferences("home", MODE_PRIVATE)
                            val edit = sharedPreferences.edit()
                            edit.putString("currenthome", user.mAccount.mHome)
                            edit.putString("currentuser", user.mAccount.mId)
                            edit.putInt("currentpermission", user.mAccount.mClassify)//0-admin
                            edit.apply()

                            val intent = Intent(applicationContext, Main2Activity::class.java)
                            intent.putExtra("id", user.mAccount.mId)
                            intent.putExtra("nameHome", user.mAccount.mHome)
                            startActivity(intent)
                        }
                    }
                    if(tmp==0){
                        Toast.makeText(applicationContext, "Đăng nhập thất bại", Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(applicationContext, "Đăng nhập thành công", Toast.LENGTH_SHORT).show()
                    }
                }

                override fun onCancelled(@NonNull databaseError: DatabaseError) {

                }
            })
        }else{
            Toast.makeText(applicationContext, "Vui lòng nhập đủ thông tin", Toast.LENGTH_SHORT).show()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 99 && resultCode == RESULT_OK) {
            edt_email.setText(data!!.getStringExtra("name"))
            edt_pass.setText(data.getStringExtra("pass"))
            Toast.makeText(applicationContext, "Đăng kí thành công", Toast.LENGTH_SHORT).show()
        }
    }

    override fun onBackPressed() {
        val startMain = Intent(Intent.ACTION_MAIN)
        startMain.addCategory(Intent.CATEGORY_HOME)
        startMain.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(startMain)
    }
}
