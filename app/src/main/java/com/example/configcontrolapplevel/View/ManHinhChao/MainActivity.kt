package com.example.configcontrolapplevel.View.ManHinhChao

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.configcontrolapplevel.R
import com.example.configcontrolapplevel.View.DangNhapDangKiActivity.DangNhapActivity
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var thread: Thread = Thread(Runnable {
            run {
                try {
                    Thread.sleep(3000)

                }catch (e: Exception){

                }finally {
                    val intent: Intent = Intent(applicationContext,DangNhapActivity::class.java)
                    startActivity(intent)
                }
            }
        })

        thread.start()
    }
}
