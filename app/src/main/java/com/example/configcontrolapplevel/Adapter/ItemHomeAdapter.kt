package com.example.configcontrolapplevel.Adapter

import android.annotation.SuppressLint
import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.configcontrolapplevel.Model.Room
import com.example.configcontrolapplevel.R

class ItemHomeAdapter(
    var context:Context?,
    var arrayList: ArrayList<Room?>, var recyclerViewClick: RecyclerViewClick) : RecyclerView.Adapter<ItemHomeAdapter.ItemHomeAdapterVH>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ItemHomeAdapterVH {
        return ItemHomeAdapterVH(LayoutInflater.from(context).inflate(R.layout.custom_home_list_item,p0,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(p0: ItemHomeAdapterVH, p1: Int) {
        val type = arrayList.get(p1)!!.mtype
        if(type.equals("Living")){
            p0.imgItem.setImageResource(R.drawable.ic_tv_white_24dp)
        }else if(type.equals("Bed")){
            p0.imgItem.setImageResource(R.drawable.ic_hotel_white_24dp)
        }else if(type.equals("Kitchen")){
            p0.imgItem.setImageResource(R.drawable.ic_kitchen_white_24dp)
        }else if(type.equals("Stair")){
            p0.imgItem.setImageResource(R.drawable.stair)
        }else if(type.equals("Gate")){
            p0.imgItem.setImageResource(R.drawable.door)
        }
        p0.txtName.setText(arrayList.get(p1)!!.mname)
        p0.txtNum.setText(arrayList.get(p1)!!.mnumberdevice.toString() + " thiết bị")
        p0.itemView.setOnClickListener {
            recyclerViewClick.onMyClick(p1)
        }
        p0.itemView.setOnLongClickListener {
            recyclerViewClick.onMyLongPress(p1)
            true
        }
    }

    class ItemHomeAdapterVH(itemView: View) : RecyclerView.ViewHolder(itemView){
        val imgItem : ImageView = itemView.findViewById(R.id.img_type)
        val txtName : TextView = itemView.findViewById(R.id.txt_name)
        val txtNum : TextView = itemView.findViewById(R.id.txt_number)
    }
}