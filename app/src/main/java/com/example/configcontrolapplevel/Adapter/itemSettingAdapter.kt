package com.example.configcontrolapplevel.Adapter

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.configcontrolapplevel.Model.ItemSetting
import com.example.configcontrolapplevel.R

class itemSettingAdapter(var context:Context?, var arrayList: ArrayList<ItemSetting>, var recyclerViewClick: RecyclerViewClick) :
    RecyclerView.Adapter<itemSettingAdapter.itemSettingAdapterVH>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): itemSettingAdapterVH {
        return itemSettingAdapterVH(LayoutInflater.from(context).inflate(R.layout.custom_setting_list_item,p0,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(p0: itemSettingAdapterVH, p1: Int) {
        p0.icon.setImageResource(arrayList.get(p1).icon)
        p0.title.setText(arrayList.get(p1).title)
        p0.itemView.setOnClickListener {
            recyclerViewClick.onMyClick(p1)
        }
    }

    class itemSettingAdapterVH(itemView: View) : RecyclerView.ViewHolder(itemView){
        val title : TextView = itemView.findViewById(R.id.txt_title_item_setting)
        val icon : ImageView = itemView.findViewById(R.id.img_icon_item_setting)
    }
}