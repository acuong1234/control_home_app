package com.example.configcontrolapplevel.Adapter

interface RecyclerViewClick {
    fun onMyClick(position:Int)
    fun onMyLongPress(position: Int)
}