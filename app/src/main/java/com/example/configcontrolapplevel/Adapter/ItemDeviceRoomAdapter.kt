package com.example.configcontrolapplevel.Adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.configcontrolapplevel.Model.Device
import com.example.configcontrolapplevel.R

class ItemDeviceRoomAdapter(var context : Context, var arrayList: ArrayList<Device>, var recyclerViewClick: RecyclerViewClick) : RecyclerView.Adapter<ItemDeviceRoomAdapter.ItemDeviceRoomAdapterVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemDeviceRoomAdapterVH {
        return ItemDeviceRoomAdapterVH(LayoutInflater.from(context).inflate(R.layout.custom_list_device_room,parent,false))
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }

    override fun onBindViewHolder(holder: ItemDeviceRoomAdapterVH, position: Int) {
        val device = arrayList.get(position)
        when(device.mType){
            0 -> holder.imgDevice.setImageResource(R.drawable.ic_lightbulb_outline_white_24dp)
            1 -> holder.imgDevice.setImageResource(R.drawable.fan)
        }
        holder.nameDevice.setText(device.mName)
        when(device.mStatus){
            0 -> {
                holder.statusDevice.setTextColor(Color.RED)
                holder.statusDevice.setText("OFF")
            }
            1 -> {
                holder.statusDevice.setTextColor(Color.GREEN)
                holder.statusDevice.setText("ON")
            }
        }
        when(device.mCountDown){
            0 -> {
                holder.countdownDevice.visibility = View.INVISIBLE
            }
            1 -> {
                holder.countdownDevice.visibility = View.VISIBLE
            }
        }
        if(device.mCountDown!=0){
            holder.countdownDevice.visibility = View.VISIBLE
        }
        holder.itemView.setOnClickListener {
            recyclerViewClick.onMyClick(position)
        }
        holder.itemView.setOnLongClickListener {
            recyclerViewClick.onMyLongPress(position)
            true
        }
    }


    class ItemDeviceRoomAdapterVH(itemView: View) : RecyclerView.ViewHolder(itemView){
        val imgDevice : ImageView = itemView.findViewById(R.id.img_device)
        val countdownDevice : ImageView = itemView.findViewById(R.id.img_timer_device)
        val nameDevice : TextView = itemView.findViewById(R.id.txt_name_device)
        val statusDevice : TextView = itemView.findViewById(R.id.txt_status_device)
    }
}