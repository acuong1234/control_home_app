package com.example.configcontrolapplevel.Model

class Device {
    var mName : String = ""
    var mType : Int = 0
    var mStatus : Int = 0
    var mCountDown : Int = 0
    var mLevel: Int = 0
    var mMode: Int = 0
    var mTime: Int = 0
}