package com.example.configcontrolapplevel.Model

class User(
    var mId: String = "",
    var mPass: String = "",
    var mName: String = "",
    var mSex: Int = 0,
    var mLocation: String = "",
    var mClassify: Int = 0,
    var mHome: String = ""
){}
