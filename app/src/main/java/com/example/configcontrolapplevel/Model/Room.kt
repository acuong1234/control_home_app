package com.example.configcontrolapplevel.Model

import java.io.Serializable

class Room : Serializable {
    var mid: Int = 0
    var mtype : String = ""
    var mname : String = ""
    var mnumberdevice : Int = 0
}
